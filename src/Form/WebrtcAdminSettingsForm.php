<?php

namespace Drupal\webrtc_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for Cloudfront credentials.
 */
class WebrtcAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webrtc_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webrtc.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('webrtc.settings');

    // IP Server.
    $form['webrtc_ip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP Server'),
      '#default_value' => $config->get('webrtc_ip'),
      '#size' => 17,
      '#maxlength' => 17,
      '#description' => $this->t('Ej: 242.32.123.23'),
      '#required' => TRUE,
    ];

    // Port.
    $form['webrtc_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#default_value' => $config->get('webrtc_port'),
      '#size' => 6,
      '#maxlength' => 6,
      '#description' => $this->t('Ej: 9000'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config('webrtc.settings')
      // Set the submitted configuration setting.
      ->set('webrtc_ip', $form_state->getValue('webrtc_ip'))
      ->set('webrtc_port', $form_state->getValue('webrtc_port'))
      ->save();
  }
}
