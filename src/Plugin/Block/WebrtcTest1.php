<?php
/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "webrtc_test1",
 *   admin_label = @Translation("WebRTC Test 1"),
 * )
 */
namespace Drupal\webrtc_integration\Plugin\Block;

use Drupal\Core\Block\BlockBase;

class WebrtcTest1 extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      '#type' => 'markup',
      '#markup' => 'This block list the article.',
    );
  }
}